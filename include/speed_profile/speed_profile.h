/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * speed_profile.h
 *
 *  Created on: Jun 26, 2014
 *      Author: Sanjiban Choudhury
 */

#ifndef SPEED_PROFILE_INCLUDE_SPEED_PROFILE_SPEED_PROFILE_H_
#define SPEED_PROFILE_INCLUDE_SPEED_PROFILE_SPEED_PROFILE_H_

#include <boost/shared_ptr.hpp>
#include <vector>

#include "speed_profile/SpeedProfile.h"

namespace ca {

class SpeedProfile {
public:
  typedef boost::shared_ptr<SpeedProfile> Ptr;
  SpeedProfile(){};
  virtual ~SpeedProfile(){};

  virtual SpeedProfile* Clone (const std::vector<double>& parameters) = 0;
  virtual double SpeedAtDistance(double s, double total_distance) const = 0;
  virtual double MaxSpeed() = 0;
  virtual std::vector<double> GetParameters() const = 0;
  virtual std::string GetName() const = 0;
};


}  // namespace ca


#endif  // SPEED_PROFILE_INCLUDE_SPEED_PROFILE_SPEED_PROFILE_H_ 
