/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * twopointacc_speed_profile.h
 *
 *  Created on: Oct 11, 2014
 *      Author: Sanjiban Choudhury
 */

#ifndef SPEED_PROFILE_INCLUDE_SPEED_PROFILE_COMMON_SPEED_PROFILES_TWOPOINTACC_SPEED_PROFILE_H_
#define SPEED_PROFILE_INCLUDE_SPEED_PROFILE_COMMON_SPEED_PROFILES_TWOPOINTACC_SPEED_PROFILE_H_

#include "speed_profile/speed_profile.h"

namespace ca {

class TwoPointAccSpeedProfile : public SpeedProfile {
 public:
  TwoPointAccSpeedProfile()
 : a1_(),
   a2_() {}

  TwoPointAccSpeedProfile(double a1, double a2)
 : a1_(a1),
   a2_(a2) {}

  virtual ~TwoPointAccSpeedProfile(){}

  virtual SpeedProfile* Clone (const std::vector<double> &parameters);

  virtual double SpeedAtDistance(double s, double total_distance) const;

  virtual std::vector<double> GetParameters() const  {
    std::vector<double> parameters = {a1_, a2_};
    return parameters;
  }

  virtual double MaxSpeed() {
    return 0; // IDK??
  }

  virtual std::string GetName() const {return "TwoPointAccSpeedProfile";}
 protected:
  double a1_, a2_;
};


}  // namespace ca


#endif  // SPEED_PROFILE_INCLUDE_SPEED_PROFILE_COMMON_SPEED_PROFILES_TWOPOINTACC_SPEED_PROFILE_H_ 
