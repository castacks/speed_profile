/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * concatenated_speed_profile.h
 *
 *  Created on: Oct 14, 2014
 *      Author: Sanjiban Choudhury
 */

#ifndef SPEED_PROFILE_INCLUDE_SPEED_PROFILE_COMMON_SPEED_PROFILES_CONCATENATED_SPEED_PROFILE_H_
#define SPEED_PROFILE_INCLUDE_SPEED_PROFILE_COMMON_SPEED_PROFILES_CONCATENATED_SPEED_PROFILE_H_

#include "speed_profile/speed_profile.h"
#include "speed_profile/common_speed_profiles/trapezoid_speed_profile.h"
#include "speed_profile/common_speed_profiles/accarray_speed_profile.h"

namespace ca {

class ConcatenatedSpeedProfile : public SpeedProfile {
public:
  ConcatenatedSpeedProfile()
: profile1_(),
  profile2_(){}

  ConcatenatedSpeedProfile(TrapezoidSpeedProfile profile1, AccArraySpeedProfile profile2) {
    profile1_ = profile1;
    profile2_ = profile2;
  }

  virtual ~ConcatenatedSpeedProfile(){}

  virtual SpeedProfile* Clone (const std::vector<double> &parameters);

  virtual double SpeedAtDistance(double s, double total_distance) const;

  virtual std::vector<double> GetParameters() const  {
    std::vector<double> parameters;
    std::vector<double> parameters1 = profile1_.GetParameters();
    parameters.insert(parameters.end(), parameters1.begin(), parameters1.end());
    std::vector<double> parameters2 = profile2_.GetParameters();
    parameters.insert(parameters.end(), parameters2.begin(), parameters2.end());
    return parameters;
  }

  virtual double MaxSpeed() {
    return std::max(profile1_.MaxSpeed(), profile2_.MaxSpeed());
  }

  virtual std::string GetName() const {return "ConcatenatedSpeedProfile";}

protected:
  TrapezoidSpeedProfile profile1_;
  AccArraySpeedProfile profile2_;
};


}  // namespace ca



#endif  // SPEED_PROFILE_INCLUDE_SPEED_PROFILE_COMMON_SPEED_PROFILES_CONCATENATED_SPEED_PROFILE_H_ 
