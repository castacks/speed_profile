/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * trapezoid_speed_profile.h
 *
 *  Created on: Jun 26, 2014
 *      Author: Sanjiban Choudhury
 */

#ifndef SPEED_PROFILE_INCLUDE_SPEED_PROFILE_COMMON_SPEED_PROFILES_TRAPEZOID_SPEED_PROFILE_H_
#define SPEED_PROFILE_INCLUDE_SPEED_PROFILE_COMMON_SPEED_PROFILES_TRAPEZOID_SPEED_PROFILE_H_

#include "speed_profile/speed_profile.h"

namespace ca {

class TrapezoidSpeedProfile : public SpeedProfile {
public:
  TrapezoidSpeedProfile()
: v1_(),
  v2_(),
  v3_(),
  a1_(),
  a3_(){}

  TrapezoidSpeedProfile(double v1, double v2, double v3, double a1, double a3);

  TrapezoidSpeedProfile(const std::vector<double> &parameters);


  virtual ~TrapezoidSpeedProfile(){}

  virtual SpeedProfile* Clone (const std::vector<double> &parameters);

  virtual double SpeedAtDistance(double s, double total_distance) const;

  virtual std::vector<double> GetParameters() const  {
    std::vector<double> parameters = {v1_, v2_, v3_, a1_, a3_};
    return parameters;
  }

  virtual double MaxSpeed() {
    return std::max(v1_, std::max(v2_, v3_));
  }


  virtual std::string GetName() const {return "TrapezoidSpeedProfile";}

protected:

  bool IsValid(double total_distance) const;

  double v1_, v2_, v3_, a1_, a3_;
};


}  // namespace ca




#endif  // SPEED_PROFILE_INCLUDE_SPEED_PROFILE_COMMON_SPEED_PROFILES_TRAPEZOID_SPEED_PROFILE_H_ 
