/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * common_factory.h
 *
 *  Created on: Oct 10, 2014
 *      Author: Sanjiban Choudhury
 */

#ifndef SPEED_PROFILE_INCLUDE_SPEED_PROFILE_COMMON_SPEED_PROFILES_COMMON_FACTORY_H_
#define SPEED_PROFILE_INCLUDE_SPEED_PROFILE_COMMON_SPEED_PROFILES_COMMON_FACTORY_H_

#include "speed_profile/speed_profile.h"
#include "speed_profile/SpeedProfile.h"
#include <map>

namespace ca {

class SpeedProfileFactory {
public:
  typedef boost::shared_ptr<SpeedProfileFactory> Ptr;

  SpeedProfileFactory();
  ~SpeedProfileFactory(){};

  SpeedProfile* Get(std::string name, const std::vector<double>& parameters) const;
  SpeedProfile* Get(const speed_profile::SpeedProfile &msg) const;
  speed_profile::SpeedProfile Get(const SpeedProfile::Ptr &obj) const;

  void Set(std::string name, SpeedProfile::Ptr profile);

protected:
  typedef std::map < std::string, SpeedProfile::Ptr > profile_type; // each entry is basically a dummy
  profile_type profiles_;
};

}




#endif  // SPEED_PROFILE_INCLUDE_SPEED_PROFILE_COMMON_SPEED_PROFILES_COMMON_FACTORY_H_ 
