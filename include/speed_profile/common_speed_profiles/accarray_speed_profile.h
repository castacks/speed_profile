/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * accarray_speed_profile.h
 *
 *  Created on: Oct 11, 2014
 *      Author: Sanjiban Choudhury
 */

#ifndef SPEED_PROFILE_INCLUDE_SPEED_PROFILE_COMMON_SPEED_PROFILES_ACCARRAY_SPEED_PROFILE_H_
#define SPEED_PROFILE_INCLUDE_SPEED_PROFILE_COMMON_SPEED_PROFILES_ACCARRAY_SPEED_PROFILE_H_

#include "speed_profile/speed_profile.h"

namespace ca {

class AccArraySpeedProfile : public SpeedProfile {
 public:
  AccArraySpeedProfile()
 : vstart_(),
   acc_(),
   length_(),
   vcap_(),
   vel_(){}

  AccArraySpeedProfile(double vstart, const std::vector<double> &acc, const std::vector<double> &length, double vcap);

  AccArraySpeedProfile(const std::vector<double> &parameters);

  virtual ~AccArraySpeedProfile(){}

  virtual SpeedProfile* Clone (const std::vector<double> &parameters);

  virtual double SpeedAtDistance(double s, double total_distance) const;

  virtual std::vector<double> GetParameters() const  {
    std::vector<double> parameters;
    parameters.push_back(vstart_);
    parameters.insert(parameters.end(), acc_.begin(), acc_.end());
    parameters.insert(parameters.end(), length_.begin(), length_.end());
    parameters.push_back(vcap_);
    return parameters;
  }

  virtual double MaxSpeed() {
    return *std::max_element(vel_.begin(),vel_.end());
  }

  virtual std::string GetName() const {return "AccArraySpeedProfile";}

  double TotalLength() const {return length_.back();}
 protected:
  double vstart_;
  std::vector<double> acc_;
  std::vector<double> length_; // cumulative (acc_.size()+1)
  double vcap_;
  std::vector<double> vel_; // cumulative (acc_.size()+1)

};

}  // namespace ca


#endif  // SPEED_PROFILE_INCLUDE_SPEED_PROFILE_COMMON_SPEED_PROFILES_ACCARRAY_SPEED_PROFILE_H_ 
