/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * speed_profile_utils.h
 *
 *  Created on: Jun 26, 2014
 *      Author: Sanjiban Choudhury
 */

#ifndef SPEED_PROFILE_INCLUDE_SPEED_PROFILE_SPEED_PROFILE_UTILS_H_
#define SPEED_PROFILE_INCLUDE_SPEED_PROFILE_SPEED_PROFILE_UTILS_H_

#include "planning_common/paths/path_waypoint.h"
#include "ca_nav_msgs/PathXYZVPsi.h"
#include "speed_profile/speed_profile.h"

namespace ca {
namespace speed_profile_utils {

/** \brief
 * Applies a speed profile. This function has been retrograded to old behaviour where speed is directly set as opposed to time scaling the trajectory.
 * Time scaling assumed a smart person was making the trajectory.
 * For now we want to maintain the dumb nature of the speed profiling.
 * This also implies that this unit will stash the speed in the first dimension.
 * @param path
 * @param speed_profile
 * @return
 */
void ApplySpeedProfile(SpeedProfile::Ptr speed_profile, planning_common::PathWaypoint &path);


void ApplyConstantSpeed(double speed, planning_common::PathWaypoint &path);

/** \brief
 * Applies speed profile to path of arbitrary length and start velocity / acceleration limit
 */
void ApplySpeedProfile(const SpeedProfile::Ptr &speed_profile, double profile_length, double start_velocity, double max_accel, planning_common::PathWaypoint &path);

/** \brief
 * Applies speed profile upto path length
 */
void ApplySpeedProfile(SpeedProfile::Ptr speed_profile, double la,  double profile_length, planning_common::PathWaypoint &path);

/** \brief
 * Applies a speed profile
 */
void ApplySpeedProfile(const SpeedProfile::Ptr &speed_profile, ca_nav_msgs::PathXYZVPsi &path);


/** \brief
 * Applies speed profile upto la.
 */
void ApplySpeedProfile(const SpeedProfile::Ptr &speed_profile, double la, double profile_length, double start_velocity, double max_accel, planning_common::PathWaypoint &path);

void DecaySpeedToZero(double max_accel, planning_common::PathWaypoint &path);

void DecaySpeedToZeroFromConstantVel(double max_accel, double constant_vel, planning_common::PathWaypoint &path);

void UniformlyInterpolateSpeed(double start_vel, double end_vel, planning_common::PathWaypoint &path);

}  // namespace speed_profile_utils
}  // namespace ca


#endif  // SPEED_PROFILE_INCLUDE_SPEED_PROFILE_SPEED_PROFILE_UTILS_H_ 
