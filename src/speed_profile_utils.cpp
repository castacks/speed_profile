/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * speed_profile_utils.cpp
 *
 *  Created on: Jun 26, 2014
 *      Author: Sanjiban Choudhury
 */

#include "speed_profile/speed_profile_utils.h"
#include "speed_profile/common_speed_profiles/uniform_speed_profile.h"
#include "planning_common/utils/workspace_utils.h"
#include "ompl/base/spaces/RealVectorStateSpace.h"
#include "planning_common/states/time_scalable_property.h"
#include "planning_common/utils/path_utils.h"
#include "planning_common/states/se3_tangent_bundle.h"
#include "math_utils/math_utils.h"
#include "geom_cast/point_cast.hpp"

#include <ros/ros.h>

namespace wu = ca::planning_common::workspace_utils;
namespace ob = ompl::base;
namespace pc = ca::planning_common;
namespace pu = pc::path_utils;
namespace nu = ca::math_utils::numeric_operations;

namespace ca {
namespace speed_profile_utils {

/*
bool ApplySpeedProfile(planning_common::PathWaypoint &path, boost::shared_ptr<SpeedProfile> speed_profile) {
  if (path.NumberOfWaypoints() == 0) {
    ROS_ERROR_STREAM("No waypoints in path while applying speed profile");
    return false;
  }

  double total_length = pu::GetWorkspaceTranslationLength(path);
  double current_length = 0;
  Eigen::VectorXd backup_position = wu::GetTranslationVectorXd(path.getSpaceInformation(), path.GetWaypoint(0));
  double current_time = wu::GetTime(path.getSpaceInformation(), path.GetWaypoint(0));

  // Cast state to time scalabe?
  const pc::TimeScalable *space_time_scalable;
  if (!(space_time_scalable = dynamic_cast<const pc::TimeScalable*> (path.getSpaceInformation()->getStateSpace().get()) )) {
    ROS_ERROR_STREAM("State not time scalable: Cant apply speed profile");
    return false;
  }

  for (std::size_t i = 0; i < path.NumberOfWaypoints(); i++) {
    Eigen::VectorXd current_position = wu::GetTranslationVectorXd(path.getSpaceInformation(), path.GetWaypoint(i));
    Eigen::VectorXd current_velocity = wu::GetTangentTranslationVectorXd(path.getSpaceInformation(), path.GetWaypoint(i));
    current_length += (current_position - backup_position).norm();

    double speed = speed_profile->SpeedAtDistance(current_length, total_length);
    std::cout<<speed<<" "<<current_velocity.norm()<<"\n";
    if (current_velocity.norm() > 0 && speed > 0) {
      std::cout<<"1\n";
      space_time_scalable->ScaleTime( path.GetWaypoint(i) , speed / current_velocity.norm());
      current_time += (current_position - backup_position).norm() / speed;
    } else if (current_velocity.norm() > 0 && speed == 0) {
      std::cout<<"2\n";
      space_time_scalable->ScaleTime( path.GetWaypoint(i) , 0);
    } else if (current_velocity.norm() ==0 && speed > 0) {
      ROS_ERROR_STREAM("Cant set non zero speed to something which was zero speed");
      return false;
    }
    wu::GetTimeState(path.getSpaceInformation(), path.GetWaypoint(i))->position = current_time;
    backup_position = current_position;
  }
  return true;
}
*/

void ApplySpeedProfile(boost::shared_ptr<SpeedProfile> speed_profile, planning_common::PathWaypoint &path) {
  if (path.NumberOfWaypoints() == 0) {
    ROS_ERROR_STREAM("No waypoints in path while applying speed profile");
    return;
  }

  double total_length = pu::GetWorkspaceTranslationLength(path);
  double current_length = 0;
  Eigen::VectorXd backup_position = wu::GetTranslationVectorXd(path.getSpaceInformation(), path.GetWaypoint(0));
  double current_time = wu::GetTime(path.getSpaceInformation(), path.GetWaypoint(0));

  for (std::size_t i = 0; i < path.NumberOfWaypoints(); i++) {
    Eigen::VectorXd current_position = wu::GetTranslationVectorXd(path.getSpaceInformation(), path.GetWaypoint(i));
    double length_segment = (current_position - backup_position).norm();
    current_length += length_segment;
    backup_position = current_position;
    double speed = speed_profile->SpeedAtDistance(current_length, total_length);
    const ob::StateSpace *space = NULL;
    ob::RealVectorStateSpace::StateType* TangentTranslation = wu::GetTangentTranslationState(path.getSpaceInformation(), path.GetWaypoint(i), space);
    for (std::size_t j = 0; j < space->getDimension(); j++)
      TangentTranslation->values[j] = 0;

    TangentTranslation->values[0] = speed;
    wu::GetTimeState(path.getSpaceInformation(), path.GetWaypoint(i))->position = current_time;
    if (speed > 0)
      current_time += length_segment / speed;

    // Extra - wiping out tangent rotation
    const ob::StateSpace *space_rot = NULL;
    ob::RealVectorStateSpace::StateType* TangentRotation = wu::GetTangentRotationState(path.getSpaceInformation(), path.GetWaypoint(i), space_rot);
    for (std::size_t j = 0; j < space_rot->getDimension(); j++)
      TangentRotation->values[j] = 0;
  }
  return;
}

void ApplyConstantSpeed(double speed, planning_common::PathWaypoint &path) {
  boost::shared_ptr<UniformSpeedProfile> prof(new UniformSpeedProfile(speed));
  return ApplySpeedProfile(prof, path);
}

void ApplySpeedProfile(const SpeedProfile::Ptr &speed_profile, ca_nav_msgs::PathXYZVPsi &path) {
  if (path.waypoints.size() < 2) {
    ROS_ERROR_STREAM("Less than 2 waypoints for speed profile");
    return;
  }

  double path_length = 0;
  for (size_t i = 1; i < path.waypoints.size(); i++) {
    path_length += (ca::point_cast<Eigen::Vector3d>(path.waypoints[i].position) - ca::point_cast<Eigen::Vector3d>(path.waypoints[i-1].position)).norm();
  }

  double current_length = 0;
  for (size_t i = 0; i < path.waypoints.size(); i++) {
    if (i >= 1)
      current_length += (ca::point_cast<Eigen::Vector3d>(path.waypoints[i].position) - ca::point_cast<Eigen::Vector3d>(path.waypoints[i-1].position)).norm();
    double speed = speed_profile->SpeedAtDistance(current_length, path_length);
    path.waypoints[i].vel = speed;
  }
}

void ApplySpeedProfile(const SpeedProfile::Ptr &speed_profile, double profile_length, double start_velocity, double max_accel, planning_common::PathWaypoint &path) {
  double path_length = path.length();
  double l_sp = std::max(profile_length, path_length);
  path.GetWaypoint(0)->as<pc::SE3TangentBundle::StateType>()->GetTangentPose().SetXYZDot(start_velocity, 0, 0);
  double running_v = start_velocity;
  double running_distance = 0;
  for (size_t i = 1; i < path.GetWaypoints().size(); i++) {
    double delta = path.getSpaceInformation()->distance(path.GetWaypoints()[i-1], path.GetWaypoints()[i]);
    running_distance += delta;
    double v_sp = speed_profile->SpeedAtDistance(l_sp - path_length + running_distance, l_sp);
    running_v = nu::SafeSqrt(running_v*running_v + 2*delta*nu::Limit(std::fabs(max_accel), nu::SafeDiv((v_sp*v_sp - running_v*running_v), 2.0*delta)));
    path.GetWaypoint(i)->as<pc::SE3TangentBundle::StateType>()->GetTangentPose().SetXYZDot(running_v, 0, 0);
  }
}

void ApplySpeedProfile(SpeedProfile::Ptr speed_profile,  double la, double profile_length, planning_common::PathWaypoint &path) {
  double path_length = path.length();
  double l_sp = std::max(la, path_length) + (profile_length - la);
  double first_part = std::max(la, path_length);
  path.GetWaypoint(0)->as<pc::SE3TangentBundle::StateType>()->GetTangentPose().SetXYZDot(speed_profile->SpeedAtDistance(0, l_sp), 0, 0);
  double running_distance = 0;
  for (std::size_t i = 1; i < path.GetWaypoints().size(); i++) {
    double delta = path.getSpaceInformation()->distance(path.GetWaypoints()[i-1], path.GetWaypoints()[i]);
    running_distance += delta;
    path.GetWaypoint(i)->as<pc::SE3TangentBundle::StateType>()->GetTangentPose().SetXYZDot(speed_profile->SpeedAtDistance(first_part - path_length + running_distance, l_sp), 0, 0);
  }
  return;
}

void ApplySpeedProfile(const SpeedProfile::Ptr &speed_profile, double la, double profile_length, double start_velocity, double max_accel, planning_common::PathWaypoint &path) {
  double path_length = path.length();
  double l_sp = std::max(la, path_length) + (profile_length - la);
  double first_part = std::max(la, path_length);
  path.GetWaypoint(0)->as<pc::SE3TangentBundle::StateType>()->GetTangentPose().SetXYZDot(start_velocity, 0, 0);
  double running_v = start_velocity;
  double running_distance = 0;
  for (size_t i = 1; i < path.GetWaypoints().size(); i++) {
    double delta = path.getSpaceInformation()->distance(path.GetWaypoints()[i-1], path.GetWaypoints()[i]);
    running_distance += delta;
    double v_sp = speed_profile->SpeedAtDistance(first_part - path_length + running_distance, l_sp);
    running_v = nu::SafeSqrt(running_v*running_v + 2*delta*nu::Limit(std::fabs(max_accel), nu::SafeDiv((v_sp*v_sp - running_v*running_v), 2.0*delta)));
    path.GetWaypoint(i)->as<pc::SE3TangentBundle::StateType>()->GetTangentPose().SetXYZDot(running_v, 0, 0);
  }
}

void DecaySpeedToZero(double max_accel, planning_common::PathWaypoint &path) {
  path.GetWaypoint(path.Size()-1)->as<pc::SE3TangentBundle::StateType>()->GetTangentPose().SetXYZDot(0, 0, 0);
  double old_v = 0;
  for (size_t i = path.GetWaypoints().size() - 1; i >=1; i--) {
    double delta = path.getSpaceInformation()->distance(path.GetWaypoints()[i-1], path.GetWaypoints()[i]);
    double new_v = sqrt(old_v*old_v + 2*max_accel*delta);
    double current_v = path.GetWaypoint(i-1)->as<pc::SE3TangentBundle::StateType>()->GetVelocity().norm();
    if (new_v < current_v)
      path.GetWaypoint(i-1)->as<pc::SE3TangentBundle::StateType>()->GetTangentPose().SetXYZDot(new_v, 0, 0);
    else
      break;
    old_v = new_v;
  }
}

void DecaySpeedToZeroFromConstantVel(double max_accel, double constant_vel, planning_common::PathWaypoint &path) {
  path.GetWaypoint(path.Size()-1)->as<pc::SE3TangentBundle::StateType>()->GetTangentPose().SetXYZDot(0, 0, 0);
  double old_v = 0;
  for (size_t i = path.GetWaypoints().size() - 1; i >=1; i--) {
    double delta = path.getSpaceInformation()->distance(path.GetWaypoints()[i-1], path.GetWaypoints()[i]);
    double new_v = sqrt(old_v*old_v + 2*max_accel*delta);
    double current_v = constant_vel;
    if (new_v < current_v)
      path.GetWaypoint(i-1)->as<pc::SE3TangentBundle::StateType>()->GetTangentPose().SetXYZDot(new_v, 0, 0);
    else
      path.GetWaypoint(i-1)->as<pc::SE3TangentBundle::StateType>()->GetTangentPose().SetXYZDot(current_v, 0, 0);
    old_v = new_v;
  }
}

void UniformlyInterpolateSpeed(double start_vel, double end_vel, planning_common::PathWaypoint &path) {
  if (path.Size() <=1 )
    return;
  path.GetWaypoint(0)->as<pc::SE3TangentBundle::StateType>()->GetTangentPose().SetXYZDot(start_vel, 0, 0);
  double path_length = path.length();
  double running_distance = 0;
  for (size_t i = 1; i < path.GetWaypoints().size(); i++) {
    running_distance += path.getSpaceInformation()->distance(path.GetWaypoints()[i-1], path.GetWaypoints()[i]);
    double new_v = start_vel + (running_distance/path_length)*(end_vel - start_vel);
    path.GetWaypoint(i)->as<pc::SE3TangentBundle::StateType>()->GetTangentPose().SetXYZDot(new_v, 0, 0);
  }
}





}  // namespace speed_profile_utils
}  // namespace ca


