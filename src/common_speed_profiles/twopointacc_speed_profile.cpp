/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * twopointacc_speed_profile.cpp
 *
 *  Created on: Oct 11, 2014
 *      Author: Sanjiban Choudhury
 */

#include "speed_profile/common_speed_profiles/twopointacc_speed_profile.h"
#include "math.h"

namespace ca {

SpeedProfile* TwoPointAccSpeedProfile::Clone (const std::vector<double> &parameters) {
  BOOST_ASSERT_MSG(parameters.size() == 2, "Twopointacc speed profile has 2 parameter");
  return new TwoPointAccSpeedProfile(parameters[0], parameters[1]);
}

double TwoPointAccSpeedProfile::SpeedAtDistance(double s, double total_distance) const {
  double speed = 0;
  if(s < 0)
    speed = 0;
  else if(s <= total_distance)
    speed = sqrt(2*a1_*s);
  else
    speed = sqrt(2*a1_*s + 2*a2_*(total_distance - s));
  return speed;
}

}


