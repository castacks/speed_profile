/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * concatenated_speed_profile.cpp
 *
 *  Created on: Oct 14, 2014
 *      Author: Sanjiban Choudhury
 */

#include "speed_profile/common_speed_profiles/concatenated_speed_profile.h"
#include <ros/ros.h>
namespace ca {


SpeedProfile* ConcatenatedSpeedProfile::Clone (const std::vector<double> &parameters) {
  std::vector<double> parameters1(parameters.begin(), parameters.begin()+5);
  std::vector<double> parameters2(parameters.begin()+5, parameters.end());
  TrapezoidSpeedProfile prof1(parameters1);
  AccArraySpeedProfile prof2(parameters2);
  return new ConcatenatedSpeedProfile(prof1, prof2);
}

double ConcatenatedSpeedProfile::SpeedAtDistance(double s, double total_distance) const {
  if (total_distance - s < profile2_.TotalLength())
    return profile2_.SpeedAtDistance(total_distance - s, profile2_.TotalLength());
  else
    return profile1_.SpeedAtDistance(s, total_distance - profile2_.TotalLength());

}

}

