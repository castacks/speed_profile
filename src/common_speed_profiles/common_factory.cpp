/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * common_factory.cpp
 *
 *  Created on: Oct 10, 2014
 *      Author: Sanjiban Choudhury
 */

#include "speed_profile/common_speed_profiles/common_factory.h"

#include <ros/ros.h>


#include "speed_profile/common_speed_profiles/uniform_speed_profile.h"
#include "speed_profile/common_speed_profiles/trapezoid_speed_profile.h"
#include "speed_profile/common_speed_profiles/twopointacc_speed_profile.h"
#include "speed_profile/common_speed_profiles/accarray_speed_profile.h"
#include "speed_profile/common_speed_profiles/concatenated_speed_profile.h"


namespace ca {

SpeedProfileFactory::SpeedProfileFactory() {
  {
    UniformSpeedProfile obj;
    profiles_[obj.GetName()] = SpeedProfile::Ptr(new UniformSpeedProfile());
  }
  {
    TrapezoidSpeedProfile obj;
    profiles_[obj.GetName()] = SpeedProfile::Ptr(new TrapezoidSpeedProfile());
  }
  {
    TwoPointAccSpeedProfile obj;
    profiles_[obj.GetName()] = SpeedProfile::Ptr(new TwoPointAccSpeedProfile());
  }
  {
    AccArraySpeedProfile obj;
    profiles_[obj.GetName()] = SpeedProfile::Ptr(new AccArraySpeedProfile());
  }
  {
    ConcatenatedSpeedProfile obj;
    profiles_[obj.GetName()] = SpeedProfile::Ptr(new ConcatenatedSpeedProfile());
  }
}

SpeedProfile* SpeedProfileFactory::Get(std::string name, const std::vector<double>& parameters) const {
  try {
    return profiles_.at(name)->Clone(parameters);
  } catch (const std::out_of_range& oor) {
    ROS_ERROR_STREAM("Invalid call to speed profile factory");
    return NULL;
  }
}

SpeedProfile* SpeedProfileFactory::Get(const speed_profile::SpeedProfile &msg) const {
  return Get(msg.name, msg.parameters);
}

speed_profile::SpeedProfile SpeedProfileFactory::Get(const SpeedProfile::Ptr &obj) const {
  speed_profile::SpeedProfile msg;
  msg.name = obj->GetName();
  msg.parameters = obj->GetParameters();
  return msg;
}


void SpeedProfileFactory::Set(std::string name, SpeedProfile::Ptr profile) {
  profiles_[name] = profile;
}

}

