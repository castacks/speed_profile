/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * uniform_speed_profile.cpp
 *
 *  Created on: Oct 10, 2014
 *      Author: Sanjiban Choudhury
 */

#include "speed_profile/common_speed_profiles/uniform_speed_profile.h"

namespace ca {

SpeedProfile* UniformSpeedProfile::Clone(const std::vector<double>& parameters) {
  BOOST_ASSERT_MSG(parameters.size() == 1, "Uniform speed profile has 1 parameter");
  return new UniformSpeedProfile(parameters[0]);
}

}


