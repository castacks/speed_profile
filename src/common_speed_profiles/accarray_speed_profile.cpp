/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * accarray_speed_profile.cpp
 *
 *  Created on: Oct 11, 2014
 *      Author: Sanjiban Choudhury
 */

#include "speed_profile/common_speed_profiles/accarray_speed_profile.h"
#include "math.h"
#include <ros/ros.h>


namespace ca {

/** Why is size of length more than acc? Why this need for initial length.
 * In all other profiles we assumed things started from 0.
 */

AccArraySpeedProfile::AccArraySpeedProfile(double vstart, const std::vector<double> &acc, const std::vector<double> &length, double vcap)
: vstart_(vstart),
  acc_(acc),
  length_(length),
  vcap_(vcap) {
  vel_.resize(length_.size());
  vel_[0] = vstart;

  for (unsigned int i = 0; i < acc_.size(); i++) {
    vel_[i+1] = sqrt(std::max(0.0, vel_[i]*vel_[i] + 2*acc_[i]*(length_[i+1] - length_[i])));
  }
}

AccArraySpeedProfile::AccArraySpeedProfile(const std::vector<double> &parameters) {
  int len = (parameters.size() - 3)/2;
  vstart_ = parameters[0];
  acc_ = std::vector<double>(parameters.begin()+1, parameters.begin()+len+1);
  length_ = std::vector<double>(parameters.begin()+len+1, parameters.begin()+2*len+1+1);
  vcap_ = parameters.back();

  vel_.resize(length_.size());
  vel_[0] = vstart_;

  for (unsigned int i = 0; i < acc_.size(); i++) {
    vel_[i+1] = sqrt(std::max(0.0, vel_[i]*vel_[i] + 2*acc_[i]*(length_[i+1] - length_[i])));
  }
}


SpeedProfile* AccArraySpeedProfile::Clone (const std::vector<double> &parameters) {
  BOOST_ASSERT_MSG(parameters.size() % 2 == 1 && parameters.size() >= 5, "Accarray speed profile must have odd parameters geq 5");
  int len = (parameters.size() - 3)/2;
  return new AccArraySpeedProfile(parameters[0], std::vector<double>(parameters.begin()+1, parameters.begin()+len+1),
                                  std::vector<double>(parameters.begin()+len+1, parameters.begin()+2*len+1+1),
                                  parameters.back());
}

double AccArraySpeedProfile::SpeedAtDistance(double s, double total_distance) const {
  if (s >= length_.back())
    return std::min(vcap_, vel_.back());
  for (unsigned int i = 0; i < length_.size() - 1; i++) {
    if (s >= length_[i] && s < length_[i+1])
      return std::min(vcap_, sqrt(std::max(0.0,vel_[i]*vel_[i] + 2*acc_[i]*(s - length_[i]))));
  }
  return 0;
}

}


