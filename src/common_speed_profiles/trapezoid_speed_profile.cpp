/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * trapezoid_speed_profile.cpp
 *
 *  Created on: Jun 26, 2014
 *      Author: Sanjiban Choudhury
 */

#include "speed_profile/common_speed_profiles/trapezoid_speed_profile.h"
#include "math.h"
namespace ca {

TrapezoidSpeedProfile::TrapezoidSpeedProfile(double v1, double v2, double v3, double a1, double a3):
  v1_(v1),
  v2_(v2),
  v3_(v3),
  a1_(a1),
  a3_(a3) {}

SpeedProfile* TrapezoidSpeedProfile::Clone (const std::vector<double> &parameters) {
  BOOST_ASSERT_MSG(parameters.size() == 5, "Trapezoid speed profile has 1 parameter");
  return new TrapezoidSpeedProfile(parameters[0], parameters[1], parameters[2], parameters[3], parameters[4]);
}

TrapezoidSpeedProfile::TrapezoidSpeedProfile(const std::vector<double> &parameters) {
  v1_ = parameters[0];
  v2_ = parameters[1];
  v3_ = parameters[2];
  a1_ = parameters[3];
  a3_ = parameters[4];
}

double TrapezoidSpeedProfile::SpeedAtDistance(double s, double total_distance) const{
  double speed = 0;
  if (s < 0) {
    speed = v1_;
  } else if (s > total_distance) {
    speed = v3_;
  } else {
    double speed1 = 0;
    if (a1_ > 0)
      speed1 = sqrt(std::min(v1_*v1_ + 2*a1_*s, v2_*v2_));
    else
      speed1 = sqrt(std::max(v1_*v1_ + 2*a1_*s, v2_*v2_));

    if (a3_ < 0 )
      speed = sqrt(std::min(speed1*speed1, v3_*v3_ - 2*a3_*(total_distance - s)));
    else
      speed = sqrt(std::max(speed1*speed1, v3_*v3_ - 2*a3_*(total_distance - s)));
  }
  return speed;
}

bool TrapezoidSpeedProfile::IsValid(double total_distance) const {
  // have to put in check here (quite messy)
  return true;
}


}  // namespace ca



