/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2015 Sanjiban Choudhury
 * speed_profile_utils_test.cpp
 *
 *  Created on: Feb 5, 2015
 *      Author: Sanjiban Choudhury
 */


// Bring in gtest
#include <gtest/gtest.h>
#include <ros/ros.h>
#include <ros/package.h>

#include "speed_profile/speed_profile_utils.h"
#include "speed_profile/common_speed_profiles/uniform_speed_profile.h"
#include "planning_common/utils/state_utils.h"

using namespace ca;

namespace pc = planning_common;
namespace su = pc::state_utils;
namespace ob = ompl::base;

TEST(ApplySpeedProfileTest, ramp_up_test) {
  SpeedProfile::Ptr sp(new UniformSpeedProfile(50.0));
  ob::SpaceInformationPtr si_se3tbsp = su::GetStandardSE3TangentBundle();
  ob::ScopedState<pc::SE3TangentBundle> start(si_se3tbsp), goal(si_se3tbsp);
  start->GetPose().setXYZ(1e3,0,0);
  goal->GetPose().setXYZ(4e3,0,0);

  pc::PathWaypoint wp_path(si_se3tbsp, start.get(), goal.get());

  speed_profile_utils::ApplySpeedProfile(sp, 4e3, 10, 0.7, wp_path);

  EXPECT_FLOAT_EQ(10, wp_path.GetWaypoint(0)->as<pc::SE3TangentBundle::StateType>()->GetVelocity().norm()) << "left vel not met";
  EXPECT_FLOAT_EQ(50, wp_path.GetWaypoint(wp_path.Size()-1)->as<pc::SE3TangentBundle::StateType>()->GetVelocity().norm()) << "left vel not met";
  EXPECT_LT(20, wp_path.GetWaypoint((int)std::ceil(0.5*wp_path.Size()))->as<pc::SE3TangentBundle::StateType>()->GetVelocity().norm()) << "mid vel should be higher";
}

// Run all the tests that were declared with TEST()
int main(int argc, char **argv) {
  ros::init(argc, argv, "speed_profile_utils_test");
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}





