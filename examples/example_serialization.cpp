/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * example_serialization.cpp
 *
 *  Created on: Oct 11, 2014
 *      Author: Sanjiban Choudhury
 */

#include <ros/ros.h>
#include "speed_profile/common_speed_profiles/twopointacc_speed_profile.h"
#include "speed_profile/common_speed_profiles/common_factory.h"

int main(int argc, char **argv) {

  speed_profile::SpeedProfile msg;
  msg.name = "AccArraySpeedProfile";
  msg.parameters = {50, -1, -0.5, 0.0, 500.0, 1000.0, 60};

  ca::SpeedProfileFactory factory;
  ca::SpeedProfile::Ptr profile_ptr(factory.Get(msg));

  if (profile_ptr) {
    for (double s = 0; s <= 1200; s+=10)
      std::cout << profile_ptr->SpeedAtDistance(s, 1200.0)<<"\n";
  }

}

