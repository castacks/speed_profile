/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2015 Sanjiban Choudhury
 * example_scratch.cpp
 *
 *  Created on: May 8, 2016
 *      Author: Sanjiban Choudhury
 */


#include "planning_common/utils/path_utils.h"
#include "planning_common/states/se3_tangent_bundle.h"
#include "planning_common/utils/state_utils.h"
#include "speed_profile/speed_profile_utils.h"

using namespace ca;

namespace pc = ca::planning_common;
namespace pu = pc::path_utils;
namespace su = pc::state_utils;
namespace ob = ompl::base;
namespace spu = speed_profile_utils;

int main(int argc, char **argv) {
  ob::SpaceInformationPtr si_se3tbsp = su::GetStandardSE3TangentBundle();

  if(1)
  {
    ob::ScopedState<pc::SE3TangentBundle> start_new(si_se3tbsp), goal_new(si_se3tbsp);
    pc::SE3TangentBundle::EigenForm start_eig, goal_eig;

    start_eig.position = Eigen::Vector3d(0,0,0);
    start_eig.velocity = Eigen::Vector3d(0,0,0);
    goal_eig.position = Eigen::Vector3d(100,0,0);
    goal_eig.velocity = Eigen::Vector3d(0,0,0);
    start_new->SetEigenForm(start_eig);
    goal_new->SetEigenForm(goal_eig);
    pc::PathWaypoint new_path(si_se3tbsp, start_new.get(), goal_new.get());
    new_path.Interpolate(100);

    spu::DecaySpeedToZeroFromConstantVel(1.0, 10.0, new_path);

    new_path.PrintAsMatrix(std::cout);
  }
}



