/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * example_trapezoid_profile.cpp
 *
 *  Created on: Oct 11, 2014
 *      Author: Sanjiban Choudhury
 */

#include <ros/ros.h>
#include "speed_profile/common_speed_profiles/trapezoid_speed_profile.h"
#include "speed_profile/common_speed_profiles/accarray_speed_profile.h"


int main(int argc, char **argv) {
  ca::TrapezoidSpeedProfile profile(30.0, 1.0, 30.0, -.1, .1);

  for (double s = 0; s <= 2000; s+=100)
    std::cout << profile.SpeedAtDistance(s, 2000.0)<<"\n";

  std::vector<double> a1 {-1, -0.5};
  std::vector<double> a2 {0.0, 500.0, 1000.0};

  ca::AccArraySpeedProfile acc_profile(60.0, a1 , a2 , 60.0);

  for (double s = 0; s <= 1200; s+=10)
    std::cout << acc_profile.SpeedAtDistance(s, 1200.0)<<"\n";

}

