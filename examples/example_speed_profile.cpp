/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * example_speed_profile.cpp
 *
 *  Created on: Jun 27, 2014
 *      Author: Sanjiban Choudhury
 */

#include "speed_profile/common_speed_profiles/trapezoid_speed_profile.h"
#include "speed_profile/speed_profile_utils.h"
#include "planning_common/states/se3_tangent_bundle.h"
#include "planning_common/utils/state_utils.h"

namespace spu = ca::speed_profile_utils;
namespace ob = ompl::base;
namespace pc = ca::planning_common;
namespace su = pc::state_utils;

int main(int argc, char **argv) {
  ob::StateSpacePtr space(new pc::SE3TangentBundle());

  ob::RealVectorBounds bounds(3);
  bounds.setLow(0);
  bounds.setHigh(1);
  space->as<pc::SE3TangentBundle>()->SetBounds(bounds, bounds, bounds);

  ob::SpaceInformationPtr si(new ob::SpaceInformation(space));

  si->setup();
  si->workspace_information().PrintInformation(std::cout);

  // Lets create a state
  ob::ScopedState<pc::SE3TangentBundle> s1(space), s2(space);
  s1->GetTime().position = 0;
  s1->GetPose().setXYZ(0,0,0);
  s1->GetTangentPose().SetXYZDot(0.1,0.1,0.1);
  su::ConvertEulerToSO3 (0, 0, 0, &s1->GetPose().rotation());
  s1->GetTangentPose().SetPhiThetaPsiDot(0.0, 0.0, 0.1);

  s2->GetTime().position = 10;
  s2->GetPose().setXYZ(1,1,1);
  s2->GetTangentPose().SetXYZDot(0.1,0.1,0.1);
  su::ConvertEulerToSO3 (0, 0, 1, &s2->GetPose().rotation());
  s2->GetTangentPose().SetPhiThetaPsiDot(0.0, 0.0, 0.1);

  pc::PathWaypoint path(si, s1.get(), s2.get());

  // Lets set 10 waypoints
  path.Interpolate(100);

  // Print old path
  std::cout << "Old path" << std::endl;
  path.PrintAsMatrix(std::cout);

  ca::SpeedProfile::Ptr speed_profile( new ca::TrapezoidSpeedProfile(0, 1, 0, 0.5, -0.5));
  spu::ApplySpeedProfile(speed_profile, path);

  // Print new path
  std::cout << "New path" << std::endl;
  path.PrintAsMatrix(std::cout);
}



